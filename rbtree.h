// header file/class for Red Black Tree
// rbtree.h

#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <cassert>
#include <algorithm>

using namespace std;

// NODE STRUCTURE
struct Node {
    int     value;
    Node    *left;
    Node    *right;
    Node    *parent;
    int     height;
    bool    red; //RED = 1 (true), BLACK = 0 (false)
    
    //Constructor
    Node(Node* newLeft = nullptr, Node* newRight = nullptr, Node* newParent = nullptr){
        left = newLeft;
        right = newRight;
        parent = newParent;
    }
    //Destructor
    ~Node(){
        delete right;
        delete left;
    }
};


// RBTREE CLASS
class RBTree {
    public: //NEEDS TO BE CHANGED TO PRIVATE AT END
        Node* root;
        bool empty;

    public:
        RBTree(){ //constructor
            root = nullptr;
        }

        ~RBTree(){ //destructor
            delete root;
        }
 

// INSERT FUNCTION       
        void insert(int value){ //insert value into tree
            
            Node* curr = root;
            Node* prev = nullptr;

            Node* newNode= new Node();
            newNode->value = value;
            newNode->red = 1;
            
            if(root == nullptr){
                root = newNode;
                newNode->parent = nullptr;
            }
            else{
                while(curr != nullptr){
                    prev = curr;
                    if(curr->value < newNode->value)
                        curr = curr->right;
                    else
                        curr = curr->left;
                }
                newNode->parent = prev;
                if(prev->value < newNode->value)
                    prev->right = newNode;
                else
                    prev->left = newNode;
            }
            fixInsert(newNode);
            newNode->height = getHeight(newNode->value);
        }


// FIX INSERT FUNCTION        
		void fixInsert(Node* curr){			
            		Node* parent = nullptr;
			Node* grandparent = nullptr;
			
			while((curr->value != root->value) && (curr->red != 0) && (curr->parent->red == 1)){
				parent = curr->parent;
				grandparent = curr->parent->parent;
				
				//Case A: parent is left child of grandparent
				if(parent == grandparent->left){
					Node* uncle = grandparent->right;
					
					//Case 1: uncle is red
					if(uncle != nullptr && uncle->red == 1){
						grandparent->red = 1;
						parent->red = 0;
						uncle->red = 0;
						curr = grandparent;
					}
                    			else if((uncle == nullptr) || (uncle->red == 1)){
						//Case 2: curr is right child or parent
						if(curr == parent->right)
						{
							rotate_left(parent);
							curr = parent;
							parent = curr->parent;
						}
						
						//Case 3: curr is left child of parent
						curr->parent->red = 0;
						grandparent->red = 1;
						rotate_right(grandparent);
					}
				}
				
				//Case B: parent is right child of grandparent
				else{
					Node* uncle = grandparent->left;
					//Case 1: uncle is red
					if((uncle != nullptr) && (uncle->red == 1)){
						grandparent->red = 1;
						parent->red = 0;
						uncle->red = 0;
						curr = grandparent;
					}
                    			else if((uncle == nullptr) || (uncle->red == 0)){
                        		//Case 2: curr is left child (right left)
                        			if(curr == parent->left){
							rotate_right(parent);
							curr = parent;
							parent = curr->parent;
						}
						
						//Case 3: curr is right child (right right)
						curr->parent->red = 0;
						grandparent->red = 1;
						rotate_left(grandparent);
                    			}
				}
			}
            root->red = 0;
            
		};
				

// PRINT FUNCTION        
        void print(){
            Node * curr = root;
            print(curr, 1);
        }
        void print(Node* curr, int indent){
            if(curr == nullptr){
                return;
            }

            print(curr->right, indent+indent);

            for(int i = 0; i < indent; i++){
                cout <<" ";}

            if(curr == root){
                cout << curr->value;
                if (curr->red == 0) cout << "(B)" << endl;
                else cout << "(R)" << endl;
            }
            else{
                cout << curr->value;
                if (curr->red == 0) cout << "(B)";
                else cout << "(R)";
                cout <<"(" << curr->parent->value << ")" << endl;
            }
            if(curr->left != nullptr){
                print(curr->left, indent+indent);
            }
        }
        

// ROTATE RIGHT FUNCTION
        Node* rotate_right(Node* target){
            if(target->left == nullptr)
                return nullptr;

            Node* temp = target->left;
            if(temp->right != nullptr){
                target->left = temp->right;
                //temp->right->parent = target;
            }
            else{
                target->left = nullptr;
            }
            if(target->parent != nullptr){
                temp->parent = target->parent;
            }
            if(target->parent == nullptr){
                root = temp;
                temp->parent = nullptr;
            }
            else{
                if(target == target->parent->left)
                    target->parent->left = temp;
                else
                    target->parent->right = temp;
            }
            temp->right = target;
            temp->height = getHeight(target->value);
            target->parent = temp;
            target->height = getHeight(target->value);
            
            return temp;
        }


// ROTATE LEFT FUNCTION
        Node* rotate_left(Node* target){
            if(target->right == nullptr)
                return nullptr;            
            
            Node* temp = target->right;
            if(temp->left != nullptr){
                target->right = temp->left;
                //temp->left->parent = target;
            }
            else{
                target->right = nullptr;
            }
            if(target->parent != nullptr){
                temp->parent = target->parent;
            }
            if(target->parent == nullptr){
                root = temp;
                temp->parent = nullptr;
            }
            else{
                if(target == target->parent->left)
                    target->parent->left = temp;
                else
                    target->parent->right = temp;
            }
            temp->left = target;
            temp->height = getHeight(target->value);
            target->parent = temp;
            target->height = getHeight(target->value);

            return temp;
            
        }


// SEARCH FUNCTION        
        Node * search(Node* curr, int key){
            //base case if node is empty
            if(curr == nullptr || curr->value == key)
                return curr;
            //recursion
            //cut search space in half
            if(key <= curr->value)
                return search(curr->left, key);
            else
                return search(curr->right, key);
        }
        
        int getHeightUtil(Node* curr, int val, int level){
            if(curr == nullptr)
                return 0;
            if(curr->value == val)
                return level;
            
            int downlevel = getHeightUtil(curr->left, val, level+1);
            if(downlevel != 0)
                return downlevel;
            downlevel = getHeightUtil(curr->right, val, level+1);
            return downlevel;
        }
        int getHeight(int val){
            Node* curr = root;
            return (getHeightUtil(curr, val, 1)-1);
        }
        
        void balanceCheckUtil(Node* curr, vector<int> &leafDist){
            if(curr->left == nullptr)
                leafDist.push_back(curr->height+1);
                
            if(curr->right == nullptr)
                leafDist.push_back(curr->height+1);
                
            if((curr->left == nullptr) && (curr->right == nullptr))
                return;
                
            if(curr->left != nullptr)
                balanceCheckUtil(curr->left, leafDist);
                
            if(curr->right != nullptr)
                balanceCheckUtil(curr->right, leafDist);
        }
        
        void balanceCheck(){
            Node* curr = root;
            vector<int> leafDist;
            balanceCheckUtil(curr, leafDist);
            int sum = 0;
            for(size_t i = 0; i < leafDist.size(); i++){
                sum = sum + leafDist[i];
            }
            
            double average = (double)sum/(double)leafDist.size();
            int maxLeafHeight = *max_element(leafDist.begin(), leafDist.end());
            int minLeafHeight = *min_element(leafDist.begin(), leafDist.end());
            double ratio = (double)maxLeafHeight / (double)minLeafHeight;
            cout <<"Sum of leaf heights is " <<sum <<endl;
            cout <<"Total number of leaves is " <<leafDist.size() <<endl;
            cout <<"Average height is " <<average <<endl;
            cout <<"The max leaf height is " <<maxLeafHeight <<endl;
            cout <<"The min leaf height is " <<minLeafHeight <<endl;
            cout <<"The ratio of the largest to smallest branch is " <<ratio <<endl;
            
        }

// SIBLING FUNCTION
        Node * sibling(Node * curr){
            if (curr == curr->parent->left) return curr->parent->right;
            else return curr->parent->left;
        }

// REPLACE FUNCTION
        void replace(Node * dest, Node * src){
            cout << endl << "--IN REPLACE FUCNTION__" << endl << endl;
            cout << "   dest: " << dest->value << endl;
            cout << "    src: " << src->value << endl;
            if (dest->parent==nullptr) root=src;
            else if (dest == dest->parent->left) dest->parent->left = src;
            else dest->parent->right = src;
            if (src) src->parent = dest->parent;

            cout << "    dest parent: " << dest->parent->value << endl;
            cout << "   dest: " << dest->value << endl;
            cout << "    src: " << src->value << endl;
        }

// IS LEAF FUNCTION
        bool is_leaf(Node * curr){
            if (curr->left==nullptr && curr->right==nullptr) return true;
            else return false;
        }
        
// ERASE FUNCTION
        void erase(){
            int num;
            Node * curr;
            curr = root;
            Node * a = nullptr;
            Node * b = nullptr;

            // input node to be deleted
            cout << "Enter element to delete: ";
            cin >> num;

            // search for element in the tree
            curr = search(root, num);
        
            // output and quit function if element is not in tree
            if (curr==nullptr){
                system("clear");
                cout << "Element " << num << " is not in the tree" << endl << endl;
                return;
            }

            // if curr has no children, make a = curr
            if(curr->left==nullptr || curr->right==nullptr) a = curr;
            else if(curr->left!=nullptr){
                a = curr->left;
                // find max value to left of curr (less than)
                while (a->right!=nullptr) a = a->right;
            }
            else{
                a = curr->right;
                // find min value to the right of curr (greater)
                while (a->left!=nullptr) a = a->left;
            }

            cout << "                   curr: " << curr->value << endl;
            cout << "                   a: " << a->value << endl;

            // Case 2
            if(a->left!=nullptr) b = a->left;
            // Case 1
            else if(a->right!=nullptr) b = a->right;  
            // Node is a leaf         
            else b = nullptr;
            
            // Set the parent of b
            if(b!=nullptr) b->parent = a->parent;

            if(a->parent==nullptr) root = b;
            else if(a==a->parent->left) a->parent->left = b;
            else a->parent->right = b;

            if(a!=curr){
                curr->red = a->red;
                curr->value = a->value;
            }   

	    if(b->red == 1 && b->parent->red == 1)
	        b->red = 0;

            if(a->red==0){
                cout << "entering erase fix" << endl;
                fixErase(b);
            }
        }


// FIX ERASE FUNCTION
        void fixErase(Node * curr){
            Node * a;
            while(curr!=root && curr->red == 0){ //&& curr->red == 0
                if(curr->parent->left==curr){ // if curr is left child
                    a = curr->parent->right;
                    if(a->red==1){
                        a->red = 0;
                        curr->parent->red = 1;
                        rotate_left(curr->parent);
                        a = curr->parent->right;
                    }
                    if(a->right->red==0 && a->left->red==0){
                        a->red = 1;
                        curr = curr->parent;
                    }
                    else{
                        if(a->right->red==0){
			    int temp = a->left->red;
                            a->left->red = a->red;
                            a->red = temp;
                            rotate_right(a);
                            a = curr->parent->right;
                        }
                        a->red = curr->parent->red;
                        curr->parent->red = 0;
                        a->right->red = 0;
                        rotate_left(curr->parent);
                        curr = root;
                    }
                }
                else{ // if curr is right child
                    a = curr->parent->left;
                    if(a->red==1){
                        a->red = 0;
                        curr->parent->red = 1;
                        rotate_right(curr->parent);
                        a = curr->parent->left;
                    }
                    if(a->left->red==0 && a->right->red==0){
                        a->red = 1;
                        curr = curr->parent;
                    }
                    else{
                        if(a->left->red==0){
			    int temp = a->right->red;
                            a->right->red = a->red;
                            a->red = temp;
                            rotate_left(a);
                            a = curr->parent->left;
                        }
                        a->red = curr->parent->red;
                        curr->parent->red = 0;
                        a->left->red = 0;
                        rotate_right(curr->parent);
                        curr = root;
                    }
                }
                curr->red = 0;
                root->red = 0;
            }
        }
                
};

