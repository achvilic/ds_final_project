CC = g++
CFLAGS = -g -Wall -std=gnu++11
TARGETS = rbtree
PROGRAMS = rbtree measure

rbtree:		rbtree.cpp
	@echo Compiling and Linking
	@$(CC) $(CFLAGS) -o $@ $^

measure:	measure.cpp
	@$(CC) $(CFLAGS) -o $@ $^


clean:
	@echo Cleaning...
	@rm -f $(TARGETS)

test:		test-output

test-output:	$(PROGRAMS)
	@echo "Testing output 1..."
	@./rbtree < input1.txt > new1.txt
	@diff output1.txt new1.txt
	@echo "Testing output 2..."
	@./rbtree < input2.txt > new2.txt
	@diff output2.txt new2.txt
	@echo "Testing output 3..."
	@./rbtree < input3.txt > new3.txt
	@diff output3.txt new3.txt

test-memory:	$(PROGRAMS)
	@echo "Testing memory..."
	@[ `valgrind --leak-check-full ./rbtree < input3.txt | grep ERROR | awk '{print $$4}'` = 0 ]
