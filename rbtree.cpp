// implementation for rbtree.h
// rbtree.cpp

#include "rbtree.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>

using namespace std;

//int main(int argc, char *argv[]){
int main(int argc, char *argv[]){
    int n, choice=1, addNum, searNum;
    int ninputs = 0;
    //RBTree b;
    if(argv[1]){
    	char* arg = argv[1];
        if(strcmp(arg, "-n")==0){
	        arg = argv[2];
    	        ninputs = stoi(arg, NULL, 10);
        }
    }


    if(ninputs != 0){
	RBTree b;
	int i = 0, tmp;
	while(cin >> tmp && i < ninputs-1){
	    cout <<tmp <<endl;
	    b.insert(tmp);
	    i++;
	}
	b.print();
    }else{
    	while(choice!=6){

            // make tree
            RBTree b;
            cout << "Number of elements: ";
            cin >> n;
            cout << endl << "Enter elements:" << endl;
            for(int i = 0; i < n; i++){
                int val;
                cin >> val;
                b.insert(val);
            }
        
            // Output menu options
            system("clear");
            b.print();
            cout << endl;
            cout << "Select an option:" << endl;
            cout << "   1. Create new tree" << endl;
            cout << "   2. Insert element" << endl;
            cout << "   3. Delete element" << endl;
            cout << "   4. Search for element" << endl;
            cout << "   5. Check balance of tree" << endl;
            cout << "   6. Quit program" << endl << endl;
            cout << "Choice: ";
            cin >> choice;

            while (choice!=1 && choice!=6){
                switch (choice){
                    case 2:
                        cout << "Enter element to insert: ";
                        cin >> addNum;
                        b.insert(addNum);
                        system("clear");
                        b.print();
                        cout << endl;
                        break;
                    case 3:
                        if (b.root==nullptr){
                            system("clear");
                            cout << "No elements to delete in tree" << endl;
                        }    
                        else{
                            b.erase();
                            system("clear");
                            b.print();
                            cout << endl;
                        }
                        break;
                    case 4:
                        cout << "Enter element to search: ";
                        cin >> searNum;
                        Node * result; 
                        system("clear");
                        result = b.search(b.root, searNum);
                        if (result==nullptr) cout << searNum << " is NOT in the tree" << endl << endl;
                        else cout << result->value << " is in the tree" << endl << endl;
                        b.print();
                        cout << endl;
                        break;
                    case 5:
                        system("clear");
                        b.balanceCheck();
                        b.print();
                        cout << endl;
                        break;
                    default:
                        system("clear");
                        cout << "Invalid entry - - - please try again" << endl << endl;
                        b.print();
                        cout << endl;
                        break;
                }

                // re-print options menu
                cout << "Select an option:" << endl;
                cout << "   1. Create new tree" << endl;
                cout << "   2. Insert element" << endl;
                cout << "   3. Delete element" << endl;
                cout << "   4. Search for element" << endl;        
                cout << "   5. Check balance of tree" << endl;
                cout << "   6. Quit program" << endl << endl;
                cout << "Choice: ";
                cin >> choice;
            }
        }
    }
    cout << endl << "Thanks for joining us!" << endl;
    return 0;
}
